# Sieci Komputerowe 2 - laboratoria - projekt #

### Temat projektu ###

Serwer memcached. Klient prosi o cos serwer, serwer oddaje z cache jak ma, jesli nie ma zapisuje i oddaje.

### Opis ogólny architektury memcached ###

Memcached używa modelu klient-serwer. Serwer przechowuje klucze i wartości jako tablice asocjacyjną, natomiast klient przetwarza tablicę i odpowiada w przetworzonej formie. Klucze maja długość do 250 bajtów, a wartości do 1 MB.

Klient używa do kontaktu z serwerem własnych bibliotek, które standardowo udostępniają usługę na porcie 11211 TCP. Każdy klient zna wszystkie serwery; serwery nie komunikują się ze sobą. Jeśli klient chce ustawić lub odczytać wartość odpowiadającą danemu kluczowi, najpierw oblicza hash klucza do określenia serwera który będzie użyty. Następnie kontaktuje się ze serwerem, który oblicza drugi hash klucza do określenia miejsca przechowywania wartości.

[wikipedia]

### Opis przyjętej architektury memcached ###

Mamy serwer główny, nazywany bazą. Podserwer przechowuje w pamięci dane pobrane wcześniej z serwera głównego. Pobiera je na żądanie od klienta, gdy brakuje ich w pamięci podręcznej, po czym przesyła je do klienta. Zakładamy, że klient zna adres podserwera, a podserwer zna adres serwera głównego, jednak klienci nie komunikują się między sobą.

### Wymagania ###
1. Kod projektu od początku utrzymywany w systemie wersjonowania git (http://dsg.cs.put.poznan.pl/gitlab) lub github/bitbucket.
2. Wszystkie aplikacje kompilują się bez zastrzeżeń - opcja kompilatora -Wall nie zgłasza żadnych błędów na komputerach laboratoryjnych (dla C lub C++).
3. Programy napisane w sposób przejrzysty i czytelny - stosowanie komentarzy, jednolitego stylu wcięć i nazewnictwa.
4. Weryfikacja programem splint przed oddaniem projektu.
5. Każdy program musi korzystać z jakiegoś systemu budowania (np. autotools, cmake, setuptools dla Pythona, qmake dla Qt itd).
6. Dołączenie krótkiego sprawozdania w formacie np. txt, czy markdown.
* treść zadania
* opis protokołu komunikacji (np. za pomocą maszyny stanów, czy pseudokodu)
* krótki opis plików źródłowych i ogólnie przyjętego modelu implementacji
* krótka informacja dotycząca używania klienta i serwera (jak włączyć, jak wyłączyć, jak skompilować, wymagania)

### Opis plików źródłowych ###
Folder `db` zawiera wszystkie dane, jakie przechowuje baza danych. Pliki w tym folderze nazywane są zgodnie z nazwą żądania, która jest shashowana przez poniższą funkcję:
```
unsigned long hash(char *str) {
  unsigned long hash = 5381;
  int c;

  while((c = *str++)) {
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  }

  return hash;
}
```
Wartość `1` odpowiada nazwie pliku `177622`.

Plik `client.c` zawiera kod programu odpowiedzialnego za obsługę klienta. Podobnie w przypadku plików `server.c` oraz `database.c`. Opis działania znajduje się w kodzie źródłowym w postaci komentarzy, a sam schemat działania przedstawia się następująco: klient przesyła klucz do serwera (klucz to shashowana nazwa żądania), serwer sprawdza czy posiada scacheowaną informację o zadanym kluczu, jeśli nie, to odpytuje o dany klucz bazę danych, baza danych zwraca informację do serwera jeśli ją posiada lub błąd 404 jeśli nie, serwer zapisuje to w pamięci podręcznej, po czym odsyła te dane do klienta.

Został przygotowany plik `makefile` do kompilacji projektu, należy go uruchomić z konsoli za pomocą komendy `make`, a następnie uruchomić programy `database`, `server` oraz `klient`. Aby pobrać istniejącą informację w bazie należy wpisać `1`, `2` lub `3` w programie klienta, pozostałe informacje nie istnieją. Aby utworzyć nową informację w bazie danych, należy ręcznie utworzyć plik w folderze `db` o nazwie odpowiadającej shashowanej nazwie informacji przez wspomnianą funkcję.

Programy działają na serwerze `localhost`, klient i podserwer na porcie `11211`, a podserwer i baza na `11222`.

Plik `.editorconfig` to plik ułatwiający utrzymanie porządku w kodzie, plik `.gitignore` do wyłączenia z repozytorium plików powstających w procesie kompilacji.