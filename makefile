CC=gcc

all:		clean client server database

client: 	client.c
			$(CC) $< -o $@ -Wall

server: 	server.c
			$(CC) -pthread $< -o $@ -Wall

database: 	database.c
			$(CC) -pthread $< -o $@ -Wall

clean:
			rm -rf client server database cache

.PHONY: 	all clean
