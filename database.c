#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread.h>

#define SERVER_PORT 11222
#define QUEUE_SIZE 5
#define BUFSIZE 1024 /* wielkosc bufora 1024 dla char = 1MB */
#define KEYSIZE 256 /* wielkosc klucza 256 dla char = 256B wynikaja z wielkosci hasha */
#define CACHEPATHSIZE 5
#define CACHEPATH "./db/"

char bufor[BUFSIZE]; /* maksymalna dlugosc bufora */
char key[KEYSIZE]; /* maksymalna dlugosc klucza */
char filename[KEYSIZE+CACHEPATHSIZE];

void* client_loop(void *arg) {
  int rcvd;
  char buffer[BUFSIZE];
  int sck = *((int*) arg);

  while((rcvd = recv(sck, buffer, BUFSIZE, 0))) {
    send(sck, buffer, rcvd, 0);
  }
  close(sck);

  pthread_exit(NULL);
}

int main(int argc, char* argv[]) {
  int nSocket, nClientSocket;
  int nBind, nListen;
  int nFoo = 1;
  socklen_t nTmp;
  struct sockaddr_in stAddr, stClientAddr;

  /* struktura adresu */
  memset(&stAddr, 0, sizeof(struct sockaddr));
  stAddr.sin_family = AF_INET;
  stAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  stAddr.sin_port = htons(SERVER_PORT);

  /* tworzenie socket */
  nSocket = socket(AF_INET, SOCK_STREAM, 0);
  if (nSocket < 0) {
    fprintf(stderr, "%s: Nie mozna utworzyc gniazda.\n", argv[0]);
    exit(1);
  }
  setsockopt(nSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&nFoo, sizeof(nFoo));

  /* przypisanie nazwy do socketu */
  nBind = bind(nSocket, (struct sockaddr*)&stAddr, sizeof(struct sockaddr));
  if (nBind < 0) {
    fprintf(stderr, "%s: Nie mozna przypisac nazwy do socketu.\n", argv[0]);
    exit(1);
  }

  /* ustalenie wielkosci kolejki */
  nListen = listen(nSocket, QUEUE_SIZE);
  if (nListen < 0) {
    fprintf(stderr, "%s:Nie mozna utworzyc kolejki o zadanej wielkosci.\n", argv[0]);
  }

  while(1) {

    /* obsluga zadania */
    nTmp = sizeof(struct sockaddr);
    pthread_t id;
    nClientSocket = accept(nSocket, (struct sockaddr*)&stClientAddr, &nTmp);
    if (nClientSocket < 0) {
      fprintf(stderr, "%s: Nie mozna dolaczyc do socketu.\n", argv[0]);
      exit(1);
    }
    pthread_create(&id, NULL, client_loop, &nTmp);
    printf("%s: [polaczono z %s] | Rozpoczeto polaczenie \n", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));

    memset(bufor, 0, sizeof bufor);
    memset(key, 0, sizeof key);

    /* odbior klucza */
    int answer;
    while((answer = read(nClientSocket, key, KEYSIZE)) > 0) {

      write(1, key, answer);

      break;

    }
    printf("\n");

    /* otwieranie bazodanowego pliku */
    struct stat st = {0};
    if (stat(CACHEPATH, &st) == -1) {
      mkdir(CACHEPATH, 0700);
    }
    strcpy(filename, CACHEPATH);
    strcat(filename, key);
    FILE *fptr;
    if((fptr=fopen(filename,"r"))==NULL) {
      printf("%s: [polaczono z %s] | ", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));
      printf("Brak zadanego pliku bazodanowego\n");
      strcpy(bufor, "Blad 404: poszukiwane informacje nie istnieja");

    } else {
      fscanf(fptr,"%[^\n]",bufor);
      printf("%s: [polaczono z %s] | ", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));
      printf("Dane z bazy danych:\n%s",bufor);
      printf("\n");
      fclose(fptr);
    }

    /* przeslanie buffera */
    write(nClientSocket, bufor, BUFSIZE);

    /* zamkniecie polaczenia */
    close(nClientSocket);
    printf("%s: [polaczono z %s] | Zakonczono polaczenie\n", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));
  }

  close(nSocket);
  return(0);
}
