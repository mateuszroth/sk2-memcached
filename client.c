#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#define BUFSIZE 1024 /* wielkosc bufora 1024 dla char = 1MB */
#define KEYSIZE 256 /* wielkosc klucza 256 dla char = 256B wynikaja z wielkosci hasha */
#define ITEMSIZE 256 /* wielkosc rzeczy 256 dla char = 256B */

char *server = "localhost"; /* adres IP petli zwrotnej */
char *protocol = "tcp"; /* typ protokolu */
short service_port = 11211;	/* port uslugi */
char bufor[BUFSIZE]; /* maksymalna dlugosc bufora */
char key[KEYSIZE]; /* maksymalna dlugosc klucza */
char item[ITEMSIZE]; /* maksymalna dlugosc itemu */

/**
 * djb2 hash function
 * http://www.cse.yorku.ca/~oz/hash.html
**/
unsigned long hash(char *str) {
  unsigned long hash = 5381;
  int c;

  while((c = *str++)) {
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  }

  return hash;
}

int main () {
	struct sockaddr_in sck_addr; /* struktura uzywana przez funkcje interfejsu gniazd */
	struct hostent *host_ptr;
	struct protoent *protocol_ptr;
	struct servent *service_ptr;

	host_ptr = gethostbyname(server);
	protocol_ptr = getprotobyname(protocol);
	service_ptr = getservbyname("daytime", protocol);

	int sck, answer; /* socket, odpowiedz z serwera */

	printf("Usluga %d na %s z serwera %s :\n", service_port, protocol, server);

  /* laczenie */
	memset(&sck_addr, 0, sizeof sck_addr);
	memcpy((char*) &sck_addr.sin_addr, (char*) host_ptr -> h_addr, host_ptr -> h_length);
	sck_addr.sin_port = service_ptr -> s_port;
	sck_addr.sin_family = AF_INET;
	inet_aton(server, &sck_addr.sin_addr);
	sck_addr.sin_port = htons(service_port);

	if((sck = socket(PF_INET, SOCK_STREAM, protocol_ptr->p_proto)) < 0) {
		perror("Nie mozna utworzyc gniazda");
		exit(EXIT_FAILURE);
	}

	if(connect(sck, (struct sockaddr*) &sck_addr, sizeof sck_addr) < 0) {
		perror("Brak polaczenia");
		exit(EXIT_FAILURE);
	}

  /* wczytywanie nazwy rzeczy do pobrania */
  printf("O co prosisz serwer?\n");
  fflush(stdin);
  scanf("%s", item);

  /* hashowanie rzeczy do klucza */
  sprintf(key, "%lu", hash(item));
  //write(1, key, KEYSIZE);

  /* wysylanie klucza do serwera */
  write(sck, key, KEYSIZE);

  /* odbieranie danych z serwera */
	while((answer = read(sck, bufor, BUFSIZE)) > 0) {
		write(1, bufor, answer);
	}

	close(sck);

  printf("\n");

	exit(EXIT_SUCCESS);
}
