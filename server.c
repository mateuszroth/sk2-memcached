#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread.h>

#define SERVER_PORT 11211
#define QUEUE_SIZE 5
#define BUFSIZE 1024 /* wielkosc bufora 1024 dla char = 1MB */
#define KEYSIZE 256 /* wielkosc klucza 256 dla char = 256B wynikaja z wielkosci hasha */
#define CACHEPATHSIZE 8
#define CACHEPATH "./cache/"

char *server = "localhost"; /* adres IP petli zwrotnej */
char *protocol = "tcp"; /* typ protokolu */
short service_port = 11222; /* port uslugi */
char bufor[BUFSIZE]; /* maksymalna dlugosc bufora */
char key[KEYSIZE]; /* maksymalna dlugosc klucza */
char filename[KEYSIZE+CACHEPATHSIZE];

void* client_loop(void *arg) {
  int rcvd;
  char buffer[BUFSIZE];
  int sck = *((int*) arg);

  while((rcvd = recv(sck, buffer, BUFSIZE, 0))) {
    send(sck, buffer, rcvd, 0);
  }
  close(sck);

  pthread_exit(NULL);
}

void database_get() {
  struct sockaddr_in sck_addr; /* struktura uzywana przez funkcje interfejsu gniazd */
  struct hostent *host_ptr;
  struct protoent *protocol_ptr;
  struct servent *service_ptr;

  host_ptr = gethostbyname(server);
  protocol_ptr = getprotobyname(protocol);
  service_ptr = getservbyname("daytime", protocol);

  int sck, answer; /* socket, odpowiedz z serwera */

  printf("Usluga %d na %s z serwera %s :\n", service_port, protocol, server);

  /* laczenie */
  memset(&sck_addr, 0, sizeof sck_addr);
  memcpy((char*) &sck_addr.sin_addr, (char*) host_ptr -> h_addr, host_ptr -> h_length);
  sck_addr.sin_port = service_ptr -> s_port;
  sck_addr.sin_family = AF_INET;
  inet_aton(server, &sck_addr.sin_addr);
  sck_addr.sin_port = htons(service_port);

  if((sck = socket(PF_INET, SOCK_STREAM, protocol_ptr->p_proto)) < 0) {
    perror("Nie mozna utworzyc gniazda");
    exit(EXIT_FAILURE);
  }

  if(connect(sck, (struct sockaddr*) &sck_addr, sizeof sck_addr) < 0) {
    perror("Brak polaczenia");
    exit(EXIT_FAILURE);
  }

  /* wysylanie klucza do bazy */
  write(sck, key, KEYSIZE);

  /* odbieranie danych z bazy */
  while((answer = read(sck, bufor, BUFSIZE)) > 0) {
    write(1, bufor, answer);
  }

  close(sck);
  printf("\n\nPobrano dane z bazy danych\n\n");
}

int main(int argc, char* argv[]) {
  int nSocket, nClientSocket;
  int nBind, nListen;
  int nFoo = 1;
  socklen_t nTmp;
  struct sockaddr_in stAddr, stClientAddr;

  /* struktura adresu */
  memset(&stAddr, 0, sizeof(struct sockaddr));
  stAddr.sin_family = AF_INET;
  stAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  stAddr.sin_port = htons(SERVER_PORT);

  /* tworzenie socket */
  nSocket = socket(AF_INET, SOCK_STREAM, 0);
  if (nSocket < 0) {
    fprintf(stderr, "%s: Nie mozna utworzyc gniazda.\n", argv[0]);
    exit(1);
  }
  setsockopt(nSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&nFoo, sizeof(nFoo));

  /* przypisanie nazwy do socketu */
  nBind = bind(nSocket, (struct sockaddr*)&stAddr, sizeof(struct sockaddr));
  if (nBind < 0) {
    fprintf(stderr, "%s: Nie mozna przypisac nazwy do socketu.\n", argv[0]);
    exit(1);
  }

  /* ustalenie wielkosci kolejki */
  nListen = listen(nSocket, QUEUE_SIZE);
  if (nListen < 0) {
    fprintf(stderr, "%s:Nie mozna utworzyc kolejki o zadanej wielkosci.\n", argv[0]);
  }

  while(1) {

    /* obsluga zadania */
    nTmp = sizeof(struct sockaddr);
    pthread_t id;
    nClientSocket = accept(nSocket, (struct sockaddr*)&stClientAddr, &nTmp);
    if (nClientSocket < 0) {
      fprintf(stderr, "%s: Nie mozna dolaczyc do socketu.\n", argv[0]);
      exit(1);
    }
    pthread_create(&id, NULL, client_loop, &nTmp);
    printf("%s: [polaczono z %s] | Rozpoczeto polaczenie \n", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));

    memset(bufor, 0, sizeof bufor);
    memset(key, 0, sizeof key);

    /* odbior klucza */
    int answer;
    while((answer = read(nClientSocket, key, KEYSIZE)) > 0) {

      write(1, key, answer);

      break;

    }
    printf("\n");

    /* otwieranie cacheowanego pliku */
    struct stat st = {0};
    if (stat(CACHEPATH, &st) == -1) {
      mkdir(CACHEPATH, 0700);
    }
    strcpy(filename, CACHEPATH);
    strcat(filename, key);
    FILE *fptr;
    if((fptr=fopen(filename,"r"))==NULL) {
      printf("%s: [polaczono z %s] | ", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));
      printf("Brak zadanego pliku cache\n");

      fptr=fopen(filename,"w");
      database_get();
      fputs(bufor, fptr);
      fclose(fptr);

      fptr=fopen(filename,"r");
      fscanf(fptr,"%[^\n]",bufor);
      fclose(fptr);

    } else {
      fscanf(fptr,"%[^\n]",bufor);
      printf("%s: [polaczono z %s] | ", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));
      printf("Dane z pliku cache:\n%s",bufor);
      printf("\n");
      fclose(fptr);
    }

    /* przeslanie buffera */
    write(nClientSocket, bufor, BUFSIZE);

    /* zamkniecie polaczenia */
    close(nClientSocket);
    printf("%s: [polaczono z %s] | Zakonczono polaczenie\n", argv[0], inet_ntoa((struct in_addr)stClientAddr.sin_addr));
  }

  close(nSocket);
  return(0);
}
